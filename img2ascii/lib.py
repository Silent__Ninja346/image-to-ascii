import numpy as np
from tqdm import tqdm
from PIL import Image


def get_tile_brightness(image):
    # getting pixel data as a 2d array
    im = np.array(image)
    # getting dimensions from np array
    width, height = im.shape
    # reshaping the np array to 1d and finding the average
    return np.average(im.reshape(width * height))


def get_tile_color(image):
    # getting pixel data as a 2d array filled with tuples
    # containing 3 or 4 numbers depending on if the image supports
    # an alpha channel or not
    im = np.array(image)
    # getting dimentions
    width = im.shape[0]
    height = im.shape[1]
    # reshaping the array to 1d
    try:
        im = im.reshape(width * height, 3)
    except ValueError:
        im = im.reshape(width * height, 4)


    # finding the average of the 1st, 2nd, and 3rd elements
    # of the tupes in each index of the array
    # this represents an average R, G, and B value for the tile
    r_avg = int(np.average(im[:, 0]))
    g_avg = int(np.average(im[:, 1]))
    b_avg = int(np.average(im[:, 2]))
    return (r_avg, g_avg, b_avg)


def run(input_path, output_path, columns, scale, color, html):
    # these characters represent every possible value to output
    # the left side is considered the darkest, the right side the brightest
    grayscale_matrix = (
        '`^",:;Il!i~+_-?][}{1)(|/tfjrxnuvczXYUJCLQ0OZmwqpdbkhao*#MW&8%B@$'
    )

    # opening the image and converting to grayscale and getting its dimensions
    try:
        im_gray = Image.open(input_path).convert(mode="L")
        im_color = Image.open(input_path)

    except IOError:
        raise Exception("Unable to open the input file")

    width, height = im_gray.size

    # splitting the image into a grid, instead of using each single pixel
    tile_width = width / columns  # width of each tile
    tile_height = tile_width / scale  # height of each tile
    rows = int(height / tile_height)  # number of rows

    # placeholder list to put each string into
    ascii_image = []

    # we want to look at small tiles of our entire image,
    # the following code generates dimentions for each tile,
    # gets the average value of those tiles, and assigns
    # an ascii character value for it

    # start by getting the y values
    # tqdm to generate a progress bar because I'm impatient
    for j in tqdm(range(rows), desc="Processing", ncols=75):
        y1 = int(j * tile_height)
        y2 = int((j + 1) * tile_height)
        # this correction is for if the number of columns is not
        # cleanly divisible prevents part of the image getting cut off
        if j == rows - 1:
            y2 = height

        # adds an empty string which we will modify later
        # add characters to as the next for loop progresses
        ascii_image.append("")

        # get x values
        for i in range(columns):
            x1 = int(i * tile_width)
            x2 = int((i + 1) * tile_width)
            # this correction is for if the number of columns is not
            # cleanly divisible prevents part of the image getting cut off
            if i == columns - 1:
                x2 = width

            # get cropped tile of the image
            cropped_gray = im_gray.crop((x1, y1, x2, y2))

            # get tile brightness as a single value
            avg = get_tile_brightness(cropped_gray)

            # get corresponding character from the grayscale matrix
            # based on the avg brightness of the tile
            character = grayscale_matrix[int((avg * 63) / 255)]

            # removing the \ character from grayscale_matrix while keeping the
            # following line fixes a bug with \ characters outputting to the
            # terminal and as html

            if character == "\\":
                character += "\\"

            if not color:
                if html:
                    ascii_image[j] += (
                        f'<span style="color:rgb(255,255'
                        f',255);">{character}</span>'
                    )
                else:
                    ascii_image[j] += character

            else:
                cropped_color = im_color.crop((x1, y1, x2, y2))
                avg_color = get_tile_color(cropped_color)

                if html:
                    character_color = (
                        f'<span style="color:rgb({avg_color[0]},{avg_color[1]}'
                        f',{avg_color[2]});">{character}</span>'
                    )

                else:
                    # the escape code for changing the color of text
                    # is the following: \033[38;2;<r>;<g>;<b>m
                    # everything after the m will have the rgb value applied
                    character_color = (
                        f"\\033[38;2;{avg_color[0]};{avg_color[1]}"
                        f";{avg_color[2]}m{character}"
                    )

                ascii_image[j] += character_color

    # if we are exporting to html, we need to add the following
    # at the start of the html document to make sure that it has
    # proper styling (black background, font, etc)
    if html:
        output_path.write(
"""
<html><code><span class="ascii" style="color: white; background: black;
display:inline-block;
white-space:pre;
letter-spacing:0;
line-height:1.4;
font-family:'Consolas','BitstreamVeraSansMono','CourierNew',Courier,monospace;
font-size:12px;
border-width:1px;
border-style:solid;
border-color:lightgray;
">""")

        output_path.seek(0, 2)

    # for each string in our previous array, write it to the file
    for row in ascii_image:
        output_path.write(row + "\n")

    # if we are exporting html, this needs to be added to the end to
    # close the tags opened in the previous if statement
    if html:
        output_path.write("</span></code></html>")

    # close the file so no portals to the fourth dimension open
    output_path.close()
